import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { FileUploadService } from '../service/file-upload.service';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styles: [],
})
export class AddBookComponent implements OnInit {
  categories = [];
  fileToUpload: File = null;
  pictureId;
  thumbNail;
  book;
  previewImg: any = 'assets/image-placeholder-big.jpg';

  constructor(
    private fb: FormBuilder,
    private httpClient: HttpClient,
    private fileUploadService: FileUploadService,
    private router: Router
  ) {}

  addBookForm = this.fb.group({
    title: ['', [Validators.required, Validators.maxLength(100)]],
    author: ['', [Validators.required, Validators.maxLength(100)]],

    category: this.fb.group({
      id: ['', Validators.required],
    }),
    description: ['', [Validators.required, Validators.maxLength(250)]],
  });

  get title() {
    return this.addBookForm.get('title');
  }

  get category() {
    return this.addBookForm.get('category');
  }

  get id() {
    return this.addBookForm.get('category.id');
  }

  get author() {
    return this.addBookForm.get('author');
  }

  get description() {
    return this.addBookForm.get('description');
  }

  ngOnInit(): void {
    this.fetchCategories();
  }

  onSubmit() {
    // this.postBook();
    this.uploadFileToActivity();
  }


  onChange(event) {
    this.fileToUpload = event.target.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);

    reader.onload = (_event) => {
      this.previewImg = reader.result;
    };
    console.log('choose file', this.fileToUpload);
  }

  uploadFileToActivity() {
    this.fileUploadService.postFile(this.fileToUpload).subscribe(
      (message: any) => {
        this.pictureId = message.url;
        console.log('get url response', message);
        this.book = {
          author: this.author.value,
          description: this.description.value,
          thumbnail: this.pictureId,
          title: this.title.value,
          category: this.category.value,
        };

        this.httpClient
          .post('http://localhost:8080/books', this.book)
          .subscribe((response) => {
            console.log(response);
          });
        Swal.fire({
          text: 'Success!',
          icon: 'success',
        });
        this.router.navigate(['/homepage']);
        console.log('uploadFiletoActiveity', this.pictureId);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  postBook() {
    console.log('file select form postbook', this.fileToUpload);
    this.pictureId = this.uploadFileToActivity();

    let book = {
      author: this.author.value,
      description: this.description.value,
      thumbnail: 'http://localhost:8080/files/' + this.pictureId,
      title: this.title.value,
      category: this.category.value,
    };

    console.log(book);

    this.httpClient
      .post('http://localhost:8080/books', book)
      .subscribe((response) => {
        console.log(response);
      });
  }

  private fetchCategories() {
    this.httpClient
      .get('http://localhost:8080/categories')
      .pipe(
        map((responseDatas) => {
          const cates = [];
          for (const k in responseDatas) {
            if (responseDatas.hasOwnProperty(k)) {
              cates.push({ ...responseDatas[k], id: k });
            }
          }
          return cates;
        })
      )
      .subscribe((cates) => {
        this.categories = cates[0].categories;
        console.log(cates[0].categories);
      });
  }
}
