import { Component, OnInit } from '@angular/core';
import { BookServiceService } from '../service/book-service.service';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-view-book',
  templateUrl: './view-book.component.html',
  styles: [],
  providers: [],
})
export class ViewBookComponent implements OnInit {
  id: number;
  sub;
  book;

  constructor(
    private bookService: BookServiceService,
    private activatedRoute: ActivatedRoute,
    private http: HttpClient
  ) {}

  ngOnInit(): void {
    this.sub = this.activatedRoute.params.subscribe((params) => {
      this.id = +params['id'];
      this.http
        .get(`http://localhost:8080/books/${this.id}`)
        .subscribe((post) => {
          this.book = post;
          console.log('fetch book by id', this.book);
          return this.book;
        });
    });
    // this.bookService.fetchPostById(id);
  }
}
