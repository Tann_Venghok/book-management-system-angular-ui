import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddBookComponent } from './add-book/add-book.component';
import { AddCategoryComponent } from './add-category/add-category.component';
import { CategoryComponent } from './category/category.component';
import { EditBookComponent } from './edit-book/edit-book.component';
import { EditCategoryComponent } from './edit-category/edit-category.component';
import { HomepageComponent } from './homepage/homepage.component';
import { ViewBookComponent } from './view-book/view-book.component';

const routes: Routes = [
  { path: '', redirectTo: '/homepage', pathMatch: 'full' },
  {
    path: 'homepage',
    component: HomepageComponent
  },
  { path: 'book/:id', component: ViewBookComponent },
  { path: 'category', component: CategoryComponent },
  { path: 'addCategory', component: AddCategoryComponent },
  { path: 'editCategory/:id', component: EditCategoryComponent },
  { path: 'editBook/:id', component: EditBookComponent },
  // {path: 'employee', component: EmployeeListComponent, canActivate: [GuardServiceService]},
  { path: 'addBook', component: AddBookComponent },
  // { path: 'employee/:number', component: EmployeeListComponent },
  // { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
