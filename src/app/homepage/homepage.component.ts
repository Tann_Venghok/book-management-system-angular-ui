import { Component, Input, OnChanges, OnInit, DoCheck } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { BookServiceService } from '../service/book-service.service';
import Swal from 'sweetalert2';
import { CategoryService } from '../service/category.service';
import { FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styles: [],
  providers: [BookServiceService],
})
export class HomepageComponent implements OnInit{
  loadedBooks = [];
  categories;
  searchText;
  searchCategory;
  page = 1;
  previewImg =
    'https://www.lotar.altervista.org/wiki/_media/wiki/plugin/bootswrapper/image-placeholder-big.jpg';

  constructor(
    private httpClient: HttpClient,
    private bookService: BookServiceService,
    private categoryService: CategoryService,
    private fb: FormBuilder,
    private translate: TranslateService
  ) {
    translate.setDefaultLang('en');
  }

  searchByCategory = this.fb.group({
    category: [''],
  });

  ngOnInit(): void {
    this.fetchPost();
    this.fetchCategories();
  }

  findBookByCategory(event) {
    let id = event.target.value;
    console.log('findByCates', id);
    this.httpClient
      .get(`http://localhost:8080/books/search/findByCategory?id=${id}`)
      .pipe(
        map((responseDatas) => {
          const posts = [];
          for (const k in responseDatas) {
            if (responseDatas.hasOwnProperty(k)) {
              posts.push({ ...responseDatas[k], id: k });
            }
          }
          return posts;
        })
      )
      .subscribe((posts) => {
        this.loadedBooks = posts[0].books;
        console.log(posts);
      });
  }

  deleteBook(id) {
    Swal.fire({
      title: 'Do you want to this delete this item?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'No',
    }).then((result) => {
      if (result.value) {
        this.httpClient
          .delete(`http://localhost:8080/books/${id}`)
          .subscribe(() => {
            window.location.reload();
          });
        // this.router.navigate(['/category']);
        Swal.fire({ title: 'Deleted!', icon: 'success' });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire('Cancelled');
      }
    });
  }

  private fetchPost() {
    this.httpClient
      .get('http://localhost:8080/books')
      .pipe(
        map((responseDatas) => {
          const posts = [];
          for (const k in responseDatas) {
            if (responseDatas.hasOwnProperty(k)) {
              posts.push({ ...responseDatas[k], id: k });
            }
          }
          return posts;
        })
      )
      .subscribe((posts) => {
        this.loadedBooks = posts[0].books;
        console.log(posts);
      });
  }

  private fetchCategories() {
    this.httpClient
      .get('http://localhost:8080/categories')
      .pipe(
        map((responseDatas) => {
          const cates = [];
          for (const k in responseDatas) {
            if (responseDatas.hasOwnProperty(k)) {
              cates.push({ ...responseDatas[k], id: k });
            }
          }
          return cates;
        })
      )
      .subscribe((cates) => {
        this.categories = cates[0].categories;
        console.log(cates[0].categories);
      });
  }
}
