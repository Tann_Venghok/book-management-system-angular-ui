import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  constructor(private http: HttpClient) {}

  postCategory(category: any) {
    this.http
      .post('http://localhost:8080/categories', category)
      .subscribe((response) => {
        console.log(response);
      });
  }

  fetchCategoryById(id) {
    this.http
      .get(`http://localhost:8080/categories/${id}`)
      .subscribe((response) => {
        console.log(response);
        return response;
      });
  }

  updateCategoryById(id, category) {
    this.http
      .put(`http://localhost:8080/categories/${id}`, category)
      .subscribe((response) => {
        console.log(response);
        return response;
      });
  }

  fetchCategories(): Observable<any> {
    // let categoryList: any;
    return this.http
      .get('http://localhost:8080/categories')
      // .pipe(map((res) => res));
    .pipe(
      map((responseDatas) => {
        const cates = [];
        for (const k in responseDatas) {
          if (responseDatas.hasOwnProperty(k)) {
            cates.push({ ...responseDatas[k], id: k });
          }
        }
        return cates;
      })
    )
    // .subscribe((cates) => {
    //   // categoryList = cates[0].categories;
    //   // console.log(cates[0].categories);
    //   console.log(cates)
    // });
    // return categoryList;
  }
}
