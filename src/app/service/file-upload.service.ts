import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class FileUploadService {
  constructor(private httpClient: HttpClient) {}

  postFile(fileToUpload: File) {
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload);
    return this.httpClient.post('http://localhost:8080/upload', formData).pipe(
      map(response => {
        return response;
      })
    );
  }

  public upload(formData) {
    return this.httpClient.post<any>('http://localhost:8080/upload', formData, {
      reportProgress: true,
      observe: 'events',
    });
  }
}
