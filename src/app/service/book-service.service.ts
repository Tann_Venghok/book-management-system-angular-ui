import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient } from "@angular/common/http"

@Injectable({
  providedIn: 'root'
})
export class BookServiceService {

  constructor(private httpClient: HttpClient) { }

  loadedBooks: [];
  book;
  fetchPost(){
    
    this.httpClient.get("http://localhost:8080/books")
    .pipe(
      map(responseDatas => {
        const posts = [];
        for(const k in responseDatas){
          if(responseDatas.hasOwnProperty(k)){
            posts.push({...responseDatas[k], id: k})
          }
        }
        return posts;
      })
    )
    .subscribe(posts => {
      this.loadedBooks = posts[0].books;
      console.log(this.loadedBooks)
      // return this.loadedBooks;
    })
    return this.loadedBooks;
  }

  fetchPostById(id){
    this.httpClient.get(`http://localhost:8080/books/${id}`)
    .subscribe(post => {
      this.book = post;
      console.log("fetch book by id",this.book)
      return this.book;
    })
    // console.log(" fetchPostById book returned", this.book)
    // return this.book
  }

  postBook(book){
    this.httpClient.post("http://localhost:8080/books", book)
    .subscribe(response =>{
      console.log(response)
    })
  }
}
