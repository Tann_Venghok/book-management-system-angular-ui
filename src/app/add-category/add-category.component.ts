import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { CategoryService } from "../service/category.service";
import {Router} from '@angular/router';
import Swal from 'sweetalert2'
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styles: [],
  providers:[]

})
export class AddCategoryComponent implements OnInit {
  category: any;
  response;

  constructor(private fb: FormBuilder, private categoryService: CategoryService, private router: Router) { }

  addCategoryForm = this.fb.group({
    title: ['', [Validators.required, Validators.maxLength(100)]],
  })

  get title(){
    return this.addCategoryForm.get('title')
  }

  ngOnInit(): void {
  }

  onSubmit(){
    this.category ={
      title: this.title.value
    }
    this.response = this.categoryService.postCategory(this.category);
    Swal.fire("Category Added Successfully");
    this.router.navigate(['/category']);
  }

}
