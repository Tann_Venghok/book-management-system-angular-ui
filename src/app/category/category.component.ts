import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styles: [],
})
export class CategoryComponent implements OnInit {
  categories;

  constructor(private http: HttpClient, private router: Router) {}

  ngOnInit(): void {
    this.fetchCategories();
  }

  deleteCategory(id) {
    Swal.fire({
      title: 'Do you want to this delete this item?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'No',
    }).then((result) => {
      if (result.value) {
        this.http.delete(`http://localhost:8080/categories/${id}`).subscribe(() => {
          window.location.reload();
        });
        // this.router.navigate(['/category']);
        Swal.fire('Deleted!', 'success');
      
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire('Cancelled');
      }
    });
  }

  private fetchCategories() {
    this.http
      .get('http://localhost:8080/categories')
      .pipe(
        map((responseDatas) => {
          const cates = [];
          for (const k in responseDatas) {
            if (responseDatas.hasOwnProperty(k)) {
              cates.push({ ...responseDatas[k], id: k });
            }
          }
          return cates;
        })
      )
      .subscribe((cates) => {
        this.categories = cates[0].categories;
        console.log(cates[0].categories);
      });
  }
}
