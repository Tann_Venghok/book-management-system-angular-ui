import { Component, OnInit, DoCheck } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { FileUploadService } from '../service/file-upload.service';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styles: [],
})
export class EditBookComponent implements OnInit{
  categories = [];
  isChangedPhoto = false;
  bookId: number;
  sub;
  book;
  fileToUpload: File = null;
  pictureId;
  previewImg: any =
    'https://www.lotar.altervista.org/wiki/_media/wiki/plugin/bootswrapper/image-placeholder-big.jpg';

  constructor(
    private activatedRoute: ActivatedRoute,
    private http: HttpClient,
    private fb: FormBuilder,
    private fileUploadService: FileUploadService,
    private router: Router
  ) {}

  addBookForm = this.fb.group({
    title: ['', [Validators.required, Validators.maxLength(100)]],
    author: ['', [Validators.required, Validators.maxLength(100)]],

    // category: this.fb.group({
    //   id: ['', [Validators.required]],
    // }),
    category: ['', [Validators.required]],
    description: ['', [Validators.required, Validators.maxLength(250)]],
  });

  get title() {
    return this.addBookForm.get('title');
  }

  get category() {
    return this.addBookForm.get('category');
  }

  // get id() {
  //   return this.addBookForm.get('category.id');
  // }

  get author() {
    return this.addBookForm.get('author');
  }

  get description() {
    return this.addBookForm.get('description');
  }

  onSubmit() {
    this.uploadFileToActivity();
    console.log(this.book);
  }

  uploadFileToActivity() {
    if (this.isChangedPhoto) {
      console.log('edit upload activity', this.fileToUpload);
      this.fileUploadService.postFile(this.fileToUpload).subscribe(
        (message: any) => {
          this.pictureId = message.url;
          console.log('get url response', message);
          this.book = {
            author: this.author.value,
            description: this.description.value,
            thumbnail: this.pictureId,
            title: this.title.value,
            category : {
              id : this.category.value
            },
          };

          this.http
            .put(`http://localhost:8080/books/${this.bookId}`, this.book)
            .subscribe((response) => {
              console.log(response);
            });
          Swal.fire({
            text: 'Updated Successfully!',
            icon: 'success',
          });
          this.router.navigate(['/homepage']);
          console.log('uploadFiletoActiveity', this.pictureId);
        },
        (error) => {
          console.log(error);
        }
      );
    } else {
      console.log("category value",this.category.value)
      this.book = {
        author: this.author.value,
        description: this.description.value,
        thumbnail: this.previewImg,
        title: this.title.value,
        category : {
          id : this.category.value
        },
        // category: this.category.value,
      };
      this.http
        .put(`http://localhost:8080/books/${this.bookId}`, this.book)
        .subscribe((response) => {
          console.log(response);
        });
      Swal.fire({
        text: 'Updated Successfully!',
        icon: 'success',
      });
      this.router.navigate(['/homepage']);
      console.log('uploadFiletoActiveity', this.pictureId);
    }
  }

  onChange(event) {
    this.isChangedPhoto = true;
    this.fileToUpload = event.target.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);

    reader.onload = (_event) => {
      this.previewImg = reader.result;
    };
    console.log('choose file', this.fileToUpload);
  }


  ngOnInit(): void {
    this.fetchCategories();
    this.sub = this.activatedRoute.params.subscribe((params) => {
      this.bookId = +params['id'];
      this.http
        .get(`http://localhost:8080/books/${this.bookId}`)
        .subscribe((post: any) => {
          console.log('fetch book by id', post);
          this.book = post;
          this.previewImg = post.thumbnail;
          this.title.setValue(this.book.title);
          this.author.setValue(this.book.author);
          this.description.setValue(this.book.description);
          this.category.setValue(this.book.category.id);

          console.log('fetch book by id', this.book);
          return this.book;
        });
    });
    // this.bookService.fetchPostById(id);
  }

  private fetchCategories() {
    this.http
      .get('http://localhost:8080/categories')
      .pipe(
        map((responseDatas) => {
          const cates = [];
          for (const k in responseDatas) {
            if (responseDatas.hasOwnProperty(k)) {
              cates.push({ ...responseDatas[k], id: k });
            }
          }
          return cates;
        })
      )
      .subscribe((cates) => {
        this.categories = cates[0].categories;
        console.log(cates[0].categories);
      });
  }
}
