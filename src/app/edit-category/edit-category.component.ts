import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { CategoryService } from '../service/category.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { HttpClient } from "@angular/common/http";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styles: [],
})
export class EditCategoryComponent implements OnInit {
  category: any;
  sub;
  categoryId;

  constructor(
    private fb: FormBuilder,
    private categoryService: CategoryService,
    private router: Router,
    private http:HttpClient,
    private activatedRoute: ActivatedRoute
  ) {}

  addCategoryForm = this.fb.group({
    title: ['', [Validators.required, Validators.maxLength(100)]],
  });

  get title() {
    return this.addCategoryForm.get('title');
  }

  ngOnInit(): void {
    this.sub = this.activatedRoute.params.subscribe((params) => {
      this.categoryId = +params['id'];
      this.http
        .get(`http://localhost:8080/categories/${this.categoryId}`)
        .subscribe((post: any) => {
          this.category = post;
          this.title.setValue(post.title);
          console.log('fetch book by id', this.category);
          return this.category;
        });
    });
  }

  onSubmit() {
    this.category = {
      title: this.title.value,
    };
    this.categoryService.updateCategoryById(this.categoryId, this.category);
    Swal.fire('Category Updated Successfully');
    this.router.navigate(['/category']);
  }
}
